# Welcome To NVIDIA AIR: DOCA Development Environment

This simulation provides a compostable DOCA development enviroment using the NVIDIA DOCA Build Environment Container.  All dependencies are preinstalled to run the DOCA development container to provide a trial development environement to get started with NVIDIA DOCA

## Mind your simulation timers

NVIDIA AIR Simulations such as this sleep after 12 hours and expire afer 36 hours by default. All data is destroyed after a simulation expires. Ensure frequent push to a remote repo or scp copy files before simulations expire. Don't lose your work

## Getting started

#### Setup SSH for the best development experience

Using SSH allows for the most robust and flexible terminal configurations to improve your CLI experience. Once SSH access is setup, it is even possible to use tools such VSCode to perform remote development on NVIDIA Air over an SSH tunnel.

SSH Access is configured for public key based authentication only. If this is your first time using NVIDIA Air, your public key will not be automatically added to the `authorized_keys` file. Take a moment to add your SSH public key to your Air acccount so that future simulations will be automatically provisioned with your SSH key for easy access.

**Manually Adding your SSH Public Key to allow SSH logins*

**Add an SSH Service to expose a port for SSH on a random TCP Port**
1. Click on the `Advanced` button on the left pane to display the simulation `Services` Panel and `Console` Panel.
2. In the `Services` Panel, 

## Setting up VSCode

Need to create a hosts file to support a custom port

## Limitations / Security

Outbound SSH is not allowed.
SSH access is by public key authenticaiton only

Steps for manually running this container locally can be found here.
https://docs.nvidia.com/doca/sdk/installation-guide/index.html#setting-up-build-environment-container-for-developers

